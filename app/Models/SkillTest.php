<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillTest extends Model
{

    protected $fillable = [
        'part_id',
        'total_user_tests'
    ];


    public function part()
    {
        return $this->belongsTo('App\Models\Part');
    }

}
