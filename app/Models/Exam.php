<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'exams';

    protected $fillable = [
        'name',
        'book_id',
        'url_audio',
        'status',
        'is_vip'
    ];

    public function book()
    {
        return $this->belongsTo('App\Models\Book');
    }

    public function parts()
    {
        return $this->hasMany('App\Models\Part');
    }
}
