<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Part extends Model
{
    protected $fillable = [
        'exam_id',
        'part_type_id',
        'url_audio'
    ];

    public function exam()
    {
        return $this->belongsTo('App\Models\Exam');
    }

    public function part_fives()
    {
        return $this->hasMany('\App\Models\PartFive');
    }

    public function part_type()
    {
        return $this->belongsTo('App\Models\PartType');
    }

    public function part_ones()
    {
        return $this->hasMany('\App\Models\PartOne');
    }
}
