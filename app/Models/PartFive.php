<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartFive extends Model
{
    protected $fillable = [
        'part_id',
        'question_id',
        'explain',
        'vocabularies',
        'translate',
        'format'
    ];

    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }
}
