<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'code_question',
        'content',
        'translate'
    ];

    public function answers()
    {
        return $this->hasMany('App\Models\Answer','question_id','id');
    }

}
