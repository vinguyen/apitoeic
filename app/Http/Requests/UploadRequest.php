<?php

namespace App\Http\Requests;

use App\Helpers\Upload;

/**
 * Class UploadRequest
 * @package App\Http\Requests
 */
class UploadRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        $rules = [];

        /**
         * check upload file
         */
        $file = $this->file('file');
        if ($file) {
            /**
             * check image upload
             */
            if (Upload::isImage($file)) {
                $rules['image'] = Upload::getImageUploadRule();
            }

            if (Upload::isMusic($file)) {
                $rules['music'] = Upload::getMusicUploadRule();
            }

        }

        return $rules;
    }
}
