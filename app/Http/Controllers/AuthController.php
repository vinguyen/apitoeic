<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\User\UserRepository;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon as Carbon;
use Laravel\Socialite\Facades\Socialite as Socialite;
use Laravel\Socialite\Two\InvalidStateException;
use Tymon\JWTAuth\Facades\JWTAuth as JWTAuth;

class AuthController extends Controller
{

    public function __construct()
    {
//        $this->middleware('auth:api', ['except' => ['login']]);
    }


    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $data = [
            'user' => Auth::user(),
            'access_token' => $token
        ];

        return response()->json($data,200);
    }

    public function register(Request $request)
    {
        $data = [
            'name'=>$request->get('name'),
            'email'=>$request->get('email'),
            'email_verified_at' => now(),
            'mobile'=>$request->get('mobile'),
            'password'=>bcrypt($request->get('password')),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];

        $userRepo = new UserRepository();

        $user = $userRepo->create($data);

        $token = $token = JWTAuth::fromUser($user);

        $data = [
            'user' => $user,
            'access_token' => $token
        ];

        return response()->json($data,200);
    }

    public function checkEmail(Request $request)
    {
        $email = $request->get('email');
        $userRepo = new UserRepository();

        $data = [
            'isEmpty' => $userRepo->findEmail($email)
        ];

        return response()->json($data,200);
    }


    public function me()
    {
        return response()->json(auth()->user());
    }


    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function updateProfile($id,Request $request)
    {

        $data = [
            'mobile'=>$request->get('mobile'),
            'email'=>$request->get('email'),
            'name'=>$request->get('name'),
        ];

        $userRepo = new UserRepository();

        $user = $userRepo->update($id, $data);

        return response()->json($user,200);

    }


    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    public function redirectToProvider($provider)
    {

        $data = [
            'redirectUrl' => Socialite::driver($provider)
                ->stateless()
                ->redirect()
                ->getTargetUrl()
        ];

        return response()->json($data, 200);
    }

    public function handleProviderCallback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch (InvalidStateException $e) {
            $user = Socialite::driver($provider)->stateless()->user();
        }

        $userRepo = new UserRepository();

        $authUser = $userRepo->findOrCreateUser($user, $provider);

        $token = JWTAuth::fromUser($authUser);

        $data = [
            'user' => $authUser,
            'access_token' => $token
        ];

        return response()->json($data,200);
    }
}
