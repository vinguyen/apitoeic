<?php

namespace App\Http\Controllers;

use App\Repositories\Exam\ExamRepository;
use App\Repositories\Part\PartRepository;
use Illuminate\Http\Request;

class ExamController extends Controller
{

    protected $examRepository;

    public function __construct(ExamRepository $examRepository)
    {
        $this->examRepository = $examRepository;
    }

    public function index(Request $request)
    {
        $book_id = $request->get('book_id');

        if($book_id) {
            return $this->examRepository->getExamsByBookId($book_id);
        }

        return $this->examRepository->getExams();
    }

    public function getPartByExam(Request $request)
    {
        $exam_id = $request->get('exam_id');
        $part_type_id = $request->get('part_type_id');

        $data = [];

        $part_repo = new PartRepository();
        $part = $part_repo->findPartByExamType($exam_id,$part_type_id);

        switch ($part_type_id) {
            case 5:
                $data = $part_repo->findQuestionPartFive($part->id);
                break;
            default:
                $data = [];
                break;
        }

        return response()->json($data,200);

    }

    public function store(Request $request)
    {
        $data = [
            'name'=>$request->get('name'),
            'book_id'=>$request->get('book_id'),
            'url_audio'=>$request->get('url_audio'),
            'status'=>$request->get('status')
        ];

        $exam = $this->examRepository->create($data);

        return [
            'success' => true,
            'data' =>[
                'id'=>$exam->id,
                'name'=>$exam->name,
            ],
            'message'=>'Upload Exam Success'
        ];
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
