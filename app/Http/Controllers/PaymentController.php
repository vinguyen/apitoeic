<?php

namespace App\Http\Controllers;

use App\Repositories\Payment\PaymentRepositoryInterface;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    protected $paymentRepository;

    public function __construct(PaymentRepositoryInterface $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function store(Request $request)
    {

        $attributes = [
            'user_id'=>$request->get('user_id'),
            'method'=>$request->get('method'),
            'amount'=>$request->get('amount'),
            'currency_code'=>$request->get('currency_code')
        ];

        $result = $this->paymentRepository->createPayment($attributes);

        return response()->json($result,200);

    }

}
