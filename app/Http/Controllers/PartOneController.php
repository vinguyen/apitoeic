<?php

namespace App\Http\Controllers;

use App\Helpers\Upload;
use App\Http\Requests\UploadRequest;
use App\Repositories\Answer\AnswerRepository;
use App\Repositories\Exam\ExamRepository;
use App\Repositories\Part\PartRepository;
use App\Repositories\PartOne\PartOneRepository;
use App\Repositories\Question\QuestionRepository;
use Illuminate\Http\Request;

class PartOneController extends Controller
{
    public function store(UploadRequest $request)
    {

        $fileAudio = $request->file('file_audio');
        $fileImage = $request->file('file_image');
        $exam_id = $request->get('exam_id');
        $code_question = $request->get('code_question');
        $answers = $request->get('answers');
        $key = $request->get('key');
        $url_audio = "";
        $url_image = "";

        $examRepo = new ExamRepository();
        $examSlug = $examRepo->getExamSlug($exam_id);

        if (gettype($request->get('answers')) == "string") {
            $answers = json_decode($answers);
        }

        if ($fileAudio) {
            if (Upload::isMusic($fileAudio)) {
                list($fileName, $filePath) = Upload::move($fileAudio,$examSlug,$code_question);
                $url_audio = $filePath;
            }
        }

        if ($fileImage) {
            if (Upload::isImage($fileImage)) {
                list($fileName, $filePath) = Upload::move($fileImage,$examSlug,$code_question);
                $url_image = $filePath;
            }
        }

        $part = new PartRepository();
        $question = new QuestionRepository();

        $part_id = $part->findPartByExamType($exam_id, 1)->id;

        $question = $question->create([
            'code_question'=>$code_question,
        ]);

        $answerRepo = new AnswerRepository();

        foreach ($answers as $answer) {
            $answerRepo->create([
                'code'=>$answer->code,
                'content'=>$answer->content,
                'is_correct'=>$answer->code==$key,
                'translate'=>$answer->translate,
                'question_id'=>$question->id
            ]);
        }

        $partOne = new PartOneRepository();

        $partOne = $partOne->create([
            'url_image'=>$url_image,
            'url_audio'=>$url_audio,
            'part_id'=>$part_id,
            'question_id'=>$question->id
        ]);

        $data = [
            'success'=>true,
            'data'=>$partOne,
        ];

        return response()->json($data,200);

    }

    public function show($id) {

        $partOneRepo = new PartOneRepository();

        return response()->json($partOneRepo->showDetailPartOne($id),200);

    }
}
