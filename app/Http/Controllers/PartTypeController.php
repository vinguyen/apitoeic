<?php

namespace App\Http\Controllers;

use App\Repositories\PartType\PartTypeRepositoryInterface;
use Illuminate\Http\Request;

class PartTypeController extends Controller
{
    protected $partTypeRepository;

    public function __construct(PartTypeRepositoryInterface $partTypeRepository)
    {
        $this->partTypeRepository = $partTypeRepository;
    }

    public function index()
    {
        $part_types = $this->partTypeRepository->getTitleName();

        return response()->json($part_types,200);

    }

    public function show($id)
    {
        $part_type = $this->partTypeRepository->find($id);

        return response()->json($part_type, 200);

    }

}
