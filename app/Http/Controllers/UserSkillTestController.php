<?php

namespace App\Http\Controllers;

use App\Repositories\SkillTest\SkillTestRepository;
use App\Repositories\UserSkillTest\UserSkillTestRepositoryInterface;
use Illuminate\Http\Request;

class UserSkillTestController extends Controller
{

    protected $userSkillTestRepository;

    public function __construct(UserSkillTestRepositoryInterface $userSkillTestRepository)
    {
        $this->userSkillTestRepository = $userSkillTestRepository;
    }

    public function create(Request $request)
    {

        $part_id = $request->get('part_id');
        $skill_test = new SkillTestRepository();
        $skill_test_id = $skill_test->findSkillTestByPart($part_id);

        $data = [
            'user_id'=>$request->get('user_id'),
            'skill_test_id'=>$skill_test_id,
            'correct_sentences'=>$request->get('correct_sentences'),
            'correct_ratio'=>$request->get('correct_ratio')
        ];

        $userSkillTest = $this->userSkillTestRepository->create($data);
        $skillTest = new SkillTestRepository();
        $skillTest->updateUserTest($skill_test_id);

        return response()->json($userSkillTest,200);

    }


    public function getSkillTestByUser(Request $request)
    {
        $user_id = $request->get('user_id');

        $data = $this->userSkillTestRepository->findSkillTestByUser($user_id);

        return response()->json($data,200);

    }

}
