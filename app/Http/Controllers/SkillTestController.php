<?php

namespace App\Http\Controllers;

use App\Repositories\SkillTest\SkillTestRepositoryInterface;
use Illuminate\Http\Request;

class SkillTestController extends Controller
{

    protected $skillTestRepository;

    public function __construct(SkillTestRepositoryInterface $skillTestRepository)
    {
        $this->skillTestRepository = $skillTestRepository;
    }

    public function create(Request $request)
    {
        $data = [
            'part_id'=>$request->get('part_id'),
            'total_user_tests'=>0
        ];

        $skillTest = $this->skillTestRepository->create($data);

        return response()->json($skillTest,200);

    }

    public function updateUserTest($id)
    {

        $result = $this->skillTestRepository->updateUserTest($id);

        return response()->json($result,200);
    }


}
