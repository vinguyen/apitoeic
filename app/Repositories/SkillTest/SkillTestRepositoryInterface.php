<?php

namespace App\Repositories\SkillTest;

interface SkillTestRepositoryInterface
{

    public function updateUserTest($id);

    public function findSkillTestByPart($part_id);

}
