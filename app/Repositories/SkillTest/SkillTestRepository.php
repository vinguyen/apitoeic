<?php

namespace App\Repositories\SkillTest;

use App\Models\SkillTest;
use App\Repositories\BaseRepository;

class SkillTestRepository extends BaseRepository implements SkillTestRepositoryInterface
{

    public function getModel()
    {
        return SkillTest::class;
    }

    public function updateUserTest($id) {

        $skillTest = $this->find($id);

        $data = [
            'part_id'=>$skillTest->part_id,
            'total_user_tests'=>$skillTest->total_user_tests+1
        ];

        return $this->update($id,$data);

    }

    public function findSkillTestByPart($part_id)
    {

        $skillTest = $this->model->where('part_id',$part_id)->first();

        return $skillTest->id;

    }
}
