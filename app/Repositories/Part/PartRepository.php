<?php

namespace App\Repositories\Part;

use App\Models\Part;
use App\Repositories\BaseRepository;
use App\Repositories\PartFive\PartFiveRepository;
use App\Repositories\PartOne\PartOneRepository;

class PartRepository extends BaseRepository implements PartRepositoryInterface {

  public function getModel()
  {
    return Part::class;
  }

  public function findPartByExamType($exam_id, $part_type_id)
  {

    $part = $this->model->where('exam_id',$exam_id)
      ->where('part_type_id',$part_type_id)
      ->first();

    if (!$part) {
      $part = $this->create([
        'exam_id'=>$exam_id,
        'part_type_id'=>$part_type_id
      ]);
    }

    return $part;

  }

  public function findExamByPartTypeId($part_type_id)
  {

    $parts = $this->model->where('part_type_id',$part_type_id)->get();

    $data = [];

    foreach ($parts as $index=>$part) {
      $item = [
        'id'=>$part->id,
        'code'=>$index+1,
        'exam'=>$part->exam->name,
        'book'=>$part->exam->book->name
      ];
      array_push($data,$item);
    }

    return $data;

  }

  public function findQuestionPartFive($part_id)
  {
    $part = $this->find($part_id);

    $part_fives = $part->part_fives;

    $questions = [];
    $part_five_repo = new PartFiveRepository();

    foreach ($part_fives as $part_five) {
      $question = $part_five_repo->showDetailPartFive($part_five->id);
      array_push($questions, $question);
    }

    $part_type = $part->part_type->title.": ".$part->part_type->name;

    return [
      'id'=>$part_id,
      'exam'=>$part->exam->name,
      'book'=>$part->exam->book->name,
      'part_type'=>$part_type,
      'direction'=>$part->part_type->direction,
      'questions'=>$questions
    ];

  }

  public function findQuestionPartOne($part_id)
  {
    $part = $this->find($part_id);

    $part_ones = $part->part_ones;

    $questions = [];
    $part_one_repo = new PartOneRepository();

    foreach ($part_ones as $part_one) {
      $question = $part_one_repo->showDetailPartOne($part_one->id);
      array_push($questions, $question);
    }

    $part_type = $part->part_type->title.": ".$part->part_type->name;

    return [
      'exam'=>$part->exam->name,
      'book'=>$part->exam->book->name,
      'part_type'=>$part_type,
      'direction'=>$part->part_type->direction,
      'questions'=>$questions
    ];

  }
}
