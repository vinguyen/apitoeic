<?php

namespace App\Repositories\Part;

interface PartRepositoryInterface {

    public function findPartByExamType($exam_id,$part_type_id);

    public function findExamByPartTypeId($part_type_id);

    public function findQuestionPartFive($part_id);

    public function findQuestionPartOne($part_id);

}
