<?php

namespace App\Repositories\PartType;

interface PartTypeRepositoryInterface
{
  public function getTitleName();
}
