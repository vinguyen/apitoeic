<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    public function getModel()
    {
        return User::class;
    }

    public function findEmail($email) {

        $user = $this->getAll()->where('email',$email)->first();

        if ($user) {
            return true;
        }
        return  false;

    }

    public function findOrCreateUser($user, $provider) {
        $authUser = $this->model->where('provider_id',$user->id)->first();

        if ($authUser) {
            return $authUser;
        }

        return $this->create([
            'name'=>$user->name,
            'email'=>$user->email,
            'provider'=>$provider,
            'provider_id'=>$user->id,
            'password'=>''
        ]);

    }

}
