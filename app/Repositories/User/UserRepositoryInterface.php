<?php

namespace App\Repositories\User;

interface UserRepositoryInterface
{
    public function findEmail($email);

    public function findOrCreateUser($user, $provider);
}
