<?php

namespace App\Repositories\PartOne;

use App\Models\PartOne;
use App\Repositories\BaseRepository;
use App\Repositories\Question\QuestionRepository;

class PartOneRepository extends BaseRepository implements PartOneRepositoryInterface
{

  public function getModel()
  {
    return PartOne::class;
  }

  public function showDetailPartOne($id) {
    $part_one = $this->find($id);

    if($part_one) {
      $question = new QuestionRepository();
      $question = $question->showDetailQuestion($part_one->question_id);

      $part_one = [
        'id'=>$question["id"],
        "code_question"=>$question["code_question"],
        "answers"=>$question["answers"],
        "key"=>$question["key"],
        "url_audio"=>$part_one->url_audio,
        "url_image"=>$part_one->url_image
      ];

      return $part_one;

    }

    return [];

  }

}
