<?php

namespace App\Repositories\Question;

interface QuestionRepositoryInterface
{

    public function showDetailQuestion($question_id);

}
