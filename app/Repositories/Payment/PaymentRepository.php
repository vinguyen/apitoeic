<?php

namespace App\Repositories\Payment;

use App\Models\Payment;
use App\Repositories\BaseRepository;
use App\Repositories\User\UserRepository;

class PaymentRepository extends BaseRepository implements PaymentRepositoryInterface
{

    public function getModel()
    {
        return Payment::class;
    }

    public function createPayment(array $attributes)
    {

        $user_id = $attributes['user_id'];

        $userRepo = new UserRepository();

        $dataUser = [
            'is_vip'=> 1
        ];

        $userRepo->update($user_id,$dataUser);

        return $this->create($attributes);
    }

}
