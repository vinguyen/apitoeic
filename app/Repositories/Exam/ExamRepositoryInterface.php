<?php

namespace App\Repositories\Exam;

interface ExamRepositoryInterface {

    public function getExams();

    public function getExamsByBookId($book_id);

    public function getExamPartFive($part_id);

    public function getExamPartOne($part_id);

    public function getExamParts($exam_id);

    public function getExamSlug($exam_id);

}
