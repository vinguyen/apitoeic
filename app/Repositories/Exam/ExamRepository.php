<?php

namespace App\Repositories\Exam;

use App\Models\Exam;
use App\Repositories\BaseRepository;
use App\Repositories\Part\PartRepository;
use App\Repositories\PartType\PartTypeRepository;
use Illuminate\Support\Str;

class ExamRepository extends BaseRepository implements ExamRepositoryInterface
{

  public function getModel()
  {
      return Exam::class;
  }

  public function getExams()
  {
    $exams = $this->getAll();

    $data = [];

    foreach ($exams as $exam) {
      $item = [
        'id'=>$exam->id,
        'name'=>$exam->name,
        'url_audio'=>$exam->url_audio,
        'status'=>$exam->status,
        'book'=>$exam->book->name
      ];
      array_push($data, $item);
    }

      return $data;
  }

  public function getExamsByBookId($book_id)
  {
      return $this->model->select(['name','id'])->where('book_id',$book_id)->get();
  }

  public function getExamPartFive($part_id)
  {
    $part_fives = new PartRepository();

    return $part_fives->findQuestionPartFive($part_id);
  }

  public function getExamPartOne($part_id)
  {

    $part_ones = new PartRepository();

    return $part_ones->findQuestionPartOne($part_id);
  }

  public function getExamParts($exam_id)
  {
    $part_types = new PartTypeRepository();
    $part_types = $part_types->getAll();
    $part = new PartRepository();

    $data = [];

    foreach ($part_types as $part_type) {
      $dataItem = $part->findPartByExamType($exam_id,$part_type->id);

      $data_part = [
        'id'=>$dataItem->id,
        'title'=>$dataItem->part_type->title,
        'name'=>$dataItem->part_type->name,
        'number_question'=>$dataItem->part_type->number_question,
        'url_audio'=>$dataItem->url_audio
      ];
      array_push($data,$data_part);
    }

    return $data;
  }

    public function getExamSlug($exam_id) {

      $exam = $this->find($exam_id);

      return [
          'name'=>Str::slug($exam->name),
          'book'=>Str::slug($exam->book->name),
          'year'=>$exam->book->year
      ];

    }

}
