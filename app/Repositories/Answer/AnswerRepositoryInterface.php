<?php

namespace App\Repositories\Answer;

interface AnswerRepositoryInterface
{
    public function findAnswerByQuestionId($question_id);

}
