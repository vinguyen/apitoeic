<?php

namespace App\Repositories\UserSkillTest;

use App\Models\UserSkillTest;
use App\Repositories\BaseRepository;

class UserSkillTestRepository extends BaseRepository implements UserSkillTestRepositoryInterface
{

    public function getModel()
    {
        return UserSkillTest::class;
    }

    public function findSkillTestByUser($user_id) {

        $userSkills = $this->model->where('user_id',$user_id)->get();
        $data = [];

        foreach ($userSkills as $userSkill) {
            $item = [
                'name_skill'=>$userSkill->skill_test->part->part_type->name,
                'title_skill'=>$userSkill->skill_test->part->part_type->title,
                'date_test'=>date_format($userSkill->created_at,"Y/m/d H:i:s"),
                'correct_sentences'=>$userSkill->correct_sentences,
                'correct_ratio'=>$userSkill->correct_ratio
            ];
            array_push($data,$item);
        }

        return $data;

    }

}
