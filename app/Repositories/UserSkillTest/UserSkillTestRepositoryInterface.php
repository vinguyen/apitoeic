<?php

namespace App\Repositories\UserSkillTest;

interface UserSkillTestRepositoryInterface
{

    public function findSkillTestByUser($user_id);

}
