<?php

namespace App\Helpers;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class Upload
{

    /**
     * allow image upload mime types
     */
    const IMAGE_MIME_TYPES = ['jpeg', 'jpg', 'gif', 'png', 'svg', 'webp'];

    /**
     * image max size
     */
    const IMAGE_UPLOAD_MAX_SIZE = 5 * 1024;

    const MP3_MINE_TYPE = ['mp3'];

    const MP3_UPLOAD_MAX_SIZE = 50 *50* 1024;

    /**
     * @return array rules
     */
    public static function getImageUploadRule()
    {
        $mimeTypes = implode(',', self::IMAGE_MIME_TYPES);

        return ['image', 'mimes:' . $mimeTypes, 'max:' . self::IMAGE_UPLOAD_MAX_SIZE];
    }

    /**
     * @return array rules
     */
    public static function getMusicUploadRule()
    {
        $mimeTypes = implode(',', self::MP3_MINE_TYPE);

        return ['image', 'mimes:' . $mimeTypes, 'max:' . self::MP3_UPLOAD_MAX_SIZE];
    }

    /**
     * @param $fileName
     * @param $fileExt
     * @param $suffix
     * @return string
     */
    protected static function getFileName($fileName, $fileExt, $suffix)
    {
        if ($suffix !== '') {
            $suffix = '-' . $suffix;
        }

        return ltrim($fileName, '/') . $suffix . $fileExt;
    }

    /**
     * @param string $path
     * @param string $fileName
     * @return bool
     */
    protected static function isFileExists($path, $fileName)
    {
        return file_exists(rtrim($path, '/') . '/' . $fileName);
    }


    /**
     * @param UploadedFile $file
     * @param array $examSlug
     * @param string $code_question
     * @return array
     */
    public static function move(UploadedFile $file,$examSlug,$code_question)
    {
        $fileExt = '.' . strtolower($file->getClientOriginalExtension());

//        $fileName = strtotime(now());

        $fileName = Str::slug($code_question);

//        $prefix = now()->format('/Y/m/d/');
        $prefix = '/'.$examSlug['year'].'/'.$examSlug['book'].'/'.$examSlug['name'].'/';
        $uploadPath = storage_path('app/public' . $prefix);

        $suffix = '';
        $targetFileName = '';

        while (true) {
            $targetFileName = static::getFileName($fileName, $fileExt, $suffix);

            $fileExisted = static::isFileExists($uploadPath, $targetFileName);
            if (! $fileExisted) {
                break;
            }

            if ($suffix === '') {
                $suffix = 0;
            } else {
                $suffix++;
            }
        }

        /**
         * upload file
         */
        $file->move($uploadPath, $targetFileName);

        return [
            $targetFileName,
            '/storage' . $prefix . $targetFileName,
        ];
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public static function isImage(UploadedFile $file)
    {
        $ext = strtolower($file->getClientOriginalExtension());

        return in_array($ext, static::IMAGE_MIME_TYPES);
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public static function isMusic(UploadedFile $file)
    {
        $ext = strtolower($file->getClientOriginalExtension());

        return in_array($ext, static::MP3_MINE_TYPE);
    }


}
