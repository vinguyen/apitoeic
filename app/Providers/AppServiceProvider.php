<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\Book\BookRepositoryInterface::class,
            \App\Repositories\Book\BookRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Exam\ExamRepositoryInterface::class,
            \App\Repositories\Exam\ExamRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Part\PartRepositoryInterface::class,
            \App\Repositories\Part\PartRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Question\QuestionRepositoryInterface::class,
            \App\Repositories\Question\QuestionRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Answer\AnswerRepositoryInterface::class,
            \App\Repositories\Answer\AnswerRepository::class
        );

        $this->app->singleton(
            \App\Repositories\PartFive\PartFiveRepositoryInterface::class,
            \App\Repositories\PartFive\PartFiveRepository::class
        );

        $this->app->singleton(
            \App\Repositories\PartType\PartTypeRepositoryInterface::class,
            \App\Repositories\PartType\PartTypeRepository::class
        );

        $this->app->singleton(
            \App\Repositories\User\UserRepositoryInterface::class,
            \App\Repositories\User\UserRepository::class
        );

        $this->app->singleton(
            \App\Repositories\SkillTest\SkillTestRepositoryInterface::class,
            \App\Repositories\SkillTest\SkillTestRepository::class
        );

        $this->app->singleton(
            \App\Repositories\UserSkillTest\UserSkillTestRepositoryInterface::class,
            \App\Repositories\UserSkillTest\UserSkillTestRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Payment\PaymentRepositoryInterface::class,
            \App\Repositories\Payment\PaymentRepository::class
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
