<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>API TOEIC</title>
</head>
<style>
    body {
        color: #4a4a4a;
        font: 20px/37px Literata,serif;
        -webkit-font-smoothing: antialiased;
    }

    h1 {
        font-size: 4.2rem;
        font-weight: 600;
        margin-bottom: 4rem;
    }

    p {
        margin: 0 0 1rem;
    }

    strong {
        font-weight: 600;
    }

    .main-content {
        padding: 32px 64px;
    }

    @media screen and (max-width: 768px) {
        .main-content {
            padding: 24px;
        }
    }

</style>
<body>
<div class="main-content" >
    <h1>Hello.</h1>
    <p><strong>My name is Vi Nguyen</strong></p>
</div>

</body>
</html>
