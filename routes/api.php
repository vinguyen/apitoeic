<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
});

Route::get('part-types','PartTypeController@index');


Route::group(['middleware' => 'jwt.auth'], function () {

    Route::get('books', 'BookController@index');

    Route::get('part-types/{id}','PartTypeController@show');

    Route::get('skill-test','ExamController@getPartByExam');

    Route::post('exams','ExamController@store');
    Route::get('exams','ExamController@index');

    Route::post('part-fives','PartFiveController@store');
    Route::post('part-ones','PartOneController@store');

    Route::get('profile', 'AuthController@me');

    Route::get('checkEmail', 'AuthController@checkEmail');

    Route::post('updateProfile/{id}','AuthController@updateProfile');

    Route::post('skill-test','SkillTestController@create');
    Route::put('skill-test/{id}','SkillTestController@updateUserTest');

    Route::post('user-skill-tests','UserSkillTestController@create');
    Route::get('skill-test-by-user','UserSkillTestController@getSkillTestByUser');

    Route::post('payments', 'PaymentController@store');

});
