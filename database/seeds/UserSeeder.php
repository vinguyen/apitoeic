<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            [
                'name'=>'User',
                'email'=>'user@gmail.com',
                'email_verified_at' => now(),
                'password'=>bcrypt('user'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'=>'Đàm Đình Tiến',
                'email'=>'tiendd@gmail.com',
                'email_verified_at' => now(),
                'password'=>bcrypt('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name'=>'Nguyễn Thị Hương',
                'email'=>'huongnt@gmail.com',
                'email_verified_at' => now(),
                'password'=>bcrypt('123456'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('users')->insert($users);

    }
}
