<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB;

class PartTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $part_type = [
            [
                'title'=>'Part 1',
                'name'=>'Photographs',
                'direction'=>'Four short statements regarding a photograph will be spoken only one time. The statements will not be printed. Of these four statements, select the one that best describes the photograph and mark your answer on the answer sheet.',
                'number_question'=>6,
                'url'=>'part-ones',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title'=>'Part 2',
                'name'=>'Question-Response',
                'direction'=>'Three responses to one question or statement will be spoken only one time. They will not be printed. Select the best response for the question, and mark your answer on the answer sheet.',
                'number_question'=>25,
                'url'=>'part-twos',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title'=>'Part 3',
                'name'=>'Conversations',
                'direction'=>'Conversations between two or three people will be spoken only one time. They will not be printed. Listen to each conversation and read the questions printed in the test book (the questions will also be spoken), select the best response for the question, and mark your answer on the answer sheet. Some questions may require responses related to information found in diagrams,etc. printed on the test book as well as what you heard in the conversations. There are three questions for each conversation.',
                'number_question'=>39,
                'url'=>'part-threes',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title'=>'Part 4',
                'name'=>'Talks',
                'direction'=>'Short talks such as announcements or narrations will be spoken only one time. They will not be printed. Listen to each talk and read the questions printed in the test book (the questions will also be spoken), select the best response for the question, and mark your answer on the answer sheet. Some questions may require responses related to information found in diagrams, etc. printed on the test book as well as what you heard in the talks. There are three questions for each talk.',
                'number_question'=>30,
                'url'=>'part-fours',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title'=>'Part 5',
                'name'=>'Incomplete Sentences',
                'direction'=>'Select the best answer of the four choices to complete the sentence, and mark your answer on the answer sheet.',
                'number_question'=>30,
                'url'=>'part-fives',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title'=>'Part 6',
                'name'=>'Text Completion',
                'direction'=>'Select the best answer of the four choices (words, phrases, or a sentence) to complete the text, and mark your answer on the answer sheet. There are four questions for each text.',
                'number_question'=>16,
                'url'=>'part-sixs',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'title'=>'Part 7',
                'name'=>'Single Passages & Multiple Passages',
                'direction'=>'A range of different texts will be printed in the test book. Read the questions, select the best answer of the four choices, and mark your answer on the answer sheet. Some questions may require you to select the best place to insert a sentence within a text. There are multiple questions for each text.',
                'number_question'=>54,
                'url'=>'part-sevens',
                'url_audio'=>'',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ];

        DB::table('part_types')->insert($part_type);

    }
}
